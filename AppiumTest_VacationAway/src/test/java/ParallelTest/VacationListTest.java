package ParallelTest;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class VacationListTest extends BaseTest {
    private String mainPage_btn_id = "com.example.quangpham_011025265:id/enterVacationList";
    private String mainPage_message_id = "com.example.quangpham_011025265:id/textView2";
    private String vacationList_recyclerView_id = "com.example.quangpham_011025265:id/vacationRecyclerView";
    private String vacationList_menuOptions_xPath = "//android.widget.ImageView[@content-desc=\"More options\"]";
    private String vacationList_menuOptions_addSampleVacation_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Add Sample Vacation\"]";
    private String vacationList_menuOptions_deleteLastVacation_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Delete Last Vacation\"]";
    private String vacationList_menuOptions_addExcursionToLastVacation_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Add Excursion to Last Vacation\"]";

    @Test(priority = 1)
    public void addSampleVacation() {

        // Main Page
        var mainPageBtn = driver.findElement(By.id(mainPage_btn_id));
        var mainPageMessage = driver.findElement(By.id(mainPage_message_id));

        Assert.assertTrue(mainPageBtn.isDisplayed());
        Assert.assertTrue(mainPageMessage.isDisplayed());

        mainPageBtn.click();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(vacationList_recyclerView_id)));

        // Vacation List
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));
        int numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();

        Assert.assertEquals(2, numOfVacation);

        var vacationList_menu = driver.findElement(By.xpath((vacationList_menuOptions_xPath)));
        vacationList_menu.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationList_menuOptions_addSampleVacation_xPath)));

        var vacationList_menuOptions_addSampleVacation = driver.findElement(By.xpath((vacationList_menuOptions_addSampleVacation_xPath)));
        vacationList_menuOptions_addSampleVacation.click();
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(4, numOfVacation);
    }

    @Test(priority = 2)
    public void deleteLastVacation() {
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));
        var vacationList_menu = driver.findElement(By.xpath((vacationList_menuOptions_xPath)));
        vacationList_menu.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationList_menuOptions_deleteLastVacation_xPath)));

        var vacationList_menuOptions_deleteLastVacation = driver.findElement(By.xpath((vacationList_menuOptions_deleteLastVacation_xPath)));
        vacationList_menuOptions_deleteLastVacation.click();
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        int numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(3, numOfVacation);
    }

    @Test(priority = 3)
    public void swipeToDeleteLastVacation() {
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));
        var lastVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).getLast();

//        ((JavascriptExecutor) driver).executeScript("mobile: dragGesture", ImmutableMap.of(
//                "elementId", ((RemoteWebElement) lastVacation).getId(),
//                "endX", -400,
//                "endY", 0
//        ));

        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "elementId", ((RemoteWebElement) lastVacation).getId(),
                "direction", "left",
                "percent", 0.75
        ));
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        int numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(2, numOfVacation);
    }

    @Test(priority = 4)
    public void deleteLastVacationHavingExcursion() {
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));
        var vacationList_menu = driver.findElement(By.xpath((vacationList_menuOptions_xPath)));
        vacationList_menu.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationList_menuOptions_deleteLastVacation_xPath)));

        var vacationList_menuOptions_addExcursionToLastVacation = driver.findElement(By.xpath((vacationList_menuOptions_addExcursionToLastVacation_xPath)));
        vacationList_menuOptions_addExcursionToLastVacation.click();
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        var lastVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).stream().toList().getLast();
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "elementId", ((RemoteWebElement) lastVacation).getId(),
                "direction", "left",
                "percent", 0.75
        ));
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        int numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(2, numOfVacation);
    }

    @Test(priority = 5)
    public void deleteFirstVacationHavingNoExcursion() {
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));

        var firstVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).stream().toList().getFirst();
        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "elementId", ((RemoteWebElement) firstVacation).getId(),
                "direction", "left",
                "percent", 0.75
        ));
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        int numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(1, numOfVacation);
    }
}
