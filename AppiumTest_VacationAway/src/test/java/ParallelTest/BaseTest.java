package ParallelTest;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.time.Duration;

public class BaseTest {
    protected AppiumDriverLocalService service;
    public AndroidDriver driver;
    public WebDriverWait driverWait;

    public BaseTest() {
    }

    @Parameters({"platformVersion", "adbSerialNum", "port"})
    @BeforeTest
    public void setUp(String platformVersion, String adbSerialNum, String port) {
        service = new AppiumServiceBuilder()
                .usingPort(Integer.parseInt(port))
                .build();
        service.start();

        if (service != null && service.isRunning()) {
            DesiredCapabilities caps = getDesiredCapabilities(platformVersion, adbSerialNum);

            driver = new AndroidDriver(service.getUrl(), caps);
            driverWait = new WebDriverWait(driver, Duration.ofSeconds(20));

            try {
                Thread.sleep(5000L);
            } catch (InterruptedException var6) {
                InterruptedException e = var6;
                e.printStackTrace();
            }

        } else {
            throw new AppiumServerHasNotBeenStartedLocallyException("Appium service node not started");
        }
    }

    @AfterTest(alwaysRun = true)
    public void afterTest() {
        if (driver != null) {
            driver.quit();
        }

        if (service != null) {
            service.stop();
        }
    }

    private static DesiredCapabilities getDesiredCapabilities(String platformVersion, String adbSerialNum) {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "android");
        caps.setCapability("automationName", "UIAutomator2");
        caps.setCapability("platformVersion", platformVersion);
        caps.setCapability("udid", adbSerialNum);
        caps.setCapability("app", "C:\\Users\\Quang Pham\\IdeaProjects\\vacation-away\\app\\build\\outputs\\apk\\debug\\app-debug.apk");
        caps.setCapability("unicodeKeyboard", true);
        caps.setCapability("resetKeyboard", true);

//        caps.setCapability("deviceName", deviceName);
//        caps.setCapability("appPackage", "");
//        caps.setCapability("appActivity", ".Settings");

        return caps;
    }
}
