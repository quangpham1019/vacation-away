package ParallelTest;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class VacationDetailsTest extends BaseTest {
    private String mainPage_btn_id = "com.example.quangpham_011025265:id/enterVacationList";
    private String mainPage_message_id = "com.example.quangpham_011025265:id/textView2";
    private String vacationList_recyclerView_id = "com.example.quangpham_011025265:id/vacationRecyclerView";
    private String vacationDetails_linearLayout_id = "com.example.quangpham_011025265:id/linearLayout";
    private String vacationDetails_editDetailsVacationName_id = "com.example.quangpham_011025265:id/editDetailsVacationName";
    private String vacationDetails_editDetailsVacationLodging_id = "com.example.quangpham_011025265:id/editDetailsVacationLodging";
    private String vacationDetails_editDetailsStartDate_id = "com.example.quangpham_011025265:id/editDetailsStartDate";
    private String vacationDetails_editDetailsEndDate_id = "com.example.quangpham_011025265:id/editDetailsEndDate";
    private String vacationDetails_deleteStartDate_id = "com.example.quangpham_011025265:id/deleteStartDate";
    private String vacationDetails_deleteEndDate_id = "com.example.quangpham_011025265:id/deleteEndDate";
    private String vacationDetails_menuOptions_xPath = "//android.widget.ImageView[@content-desc=\"More options\"]";
    private String vacationDetails_excursionRecyclerView_id = "com.example.quangpham_011025265:id/excursionRecyclerView";
    private String vacationDetails_addExcursion_id = "com.example.quangpham_011025265:id/addExcursion";
    private String vacationDetails_menuOptions_saveVacation_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Save Vacation\"]";
    private String vacationDetails_menuOptions_notifyOnStartDate_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Notify on Start Date\"]";
    private String vacationDetails_menuOptions_notifyOnEndDate_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Notify on End Date\"]";
    private String vacationDetails_menuOptions_shareYourVacationPlan_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Share your vacation plan\"]";
    private String vacationDetails_menuOptions_addSampleExcursion_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Add a sample excursion\"]";

    @Test(priority = 1)
    public void allVacationDetailsDisplayedTest() {
        // all component are displayed
        // vacationName matches name shown in vacationList

        // Main Page
        var mainPageBtn = driver.findElement(By.id(mainPage_btn_id));
        mainPageBtn.click();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(vacationList_recyclerView_id)));

        // Vacation List
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));
        var lastVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).getLast();
        String curVacationNameInVacationList = lastVacation.getAttribute("text");
        lastVacation.click();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(vacationDetails_linearLayout_id)));

        // Vacation Details
        var vacationDetails_vacationName = driver.findElement(By.id(vacationDetails_editDetailsVacationName_id));
        var vacationDetails_vacationLodging = driver.findElement(By.id(vacationDetails_editDetailsVacationLodging_id));
        var vacationDetails_startDate = driver.findElement(By.id(vacationDetails_editDetailsStartDate_id));
        var vacationDetails_endDate = driver.findElement(By.id(vacationDetails_editDetailsEndDate_id));

        Assert.assertTrue(vacationDetails_vacationName.isDisplayed());
        Assert.assertTrue(vacationDetails_vacationLodging.isDisplayed());
        Assert.assertTrue(vacationDetails_startDate.isDisplayed());
        Assert.assertTrue(vacationDetails_endDate.isDisplayed());

        Assert.assertEquals(curVacationNameInVacationList, vacationDetails_vacationName.getAttribute("text"));
    }

    @Test(priority = 2)
    public void addSampleExcursionTest() {
        // press "Add a sample excursion" in menu options
        // total number of excursion should increase by 1

        var vacationDetails_excursionRecyclerView = driver.findElement(By.id(vacationDetails_excursionRecyclerView_id));
        int initialNumOfAssociatedExcursion = vacationDetails_excursionRecyclerView.findElements(By.className("android.widget.TextView")).size();

        var vacationDetails_menuOptions = driver.findElement(By.xpath(vacationDetails_menuOptions_xPath));
        vacationDetails_menuOptions.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationDetails_menuOptions_addSampleExcursion_xPath)));

        var vacationDetails_menuOptions_addSampleExcursion = driver.findElement(By.xpath(vacationDetails_menuOptions_addSampleExcursion_xPath));
        vacationDetails_menuOptions_addSampleExcursion.click();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(vacationDetails_excursionRecyclerView_id)));

        vacationDetails_excursionRecyclerView = driver.findElement(By.id(vacationDetails_excursionRecyclerView_id));
        int newNumOfAssociatedExcursion = vacationDetails_excursionRecyclerView.findElements(By.className("android.widget.TextView")).size();

        Assert.assertEquals(newNumOfAssociatedExcursion, initialNumOfAssociatedExcursion + 1);
    }

    @Test(priority = 3)
    public void deleteSampleExcursionTest() {
        // add another sample excursion, total number of excursion should increase by 1
        // swipe left on the sample excursion, total number of excursion should decrease by 1

        var vacationDetails_excursionRecyclerView = driver.findElement(By.id(vacationDetails_excursionRecyclerView_id));
        int initialNumOfAssociatedExcursion = vacationDetails_excursionRecyclerView.findElements(By.className("android.widget.TextView")).size();

        var vacationDetails_menuOptions = driver.findElement(By.xpath(vacationDetails_menuOptions_xPath));
        vacationDetails_menuOptions.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationDetails_menuOptions_addSampleExcursion_xPath)));

        var vacationDetails_menuOptions_addSampleExcursion = driver.findElement(By.xpath(vacationDetails_menuOptions_addSampleExcursion_xPath));
        vacationDetails_menuOptions_addSampleExcursion.click();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(vacationDetails_excursionRecyclerView_id)));

        vacationDetails_excursionRecyclerView = driver.findElement(By.id(vacationDetails_excursionRecyclerView_id));
        int numOfAssociatedExcursionsAfterAddSample = vacationDetails_excursionRecyclerView.findElements(By.className("android.widget.TextView")).size();

        Assert.assertEquals(numOfAssociatedExcursionsAfterAddSample, initialNumOfAssociatedExcursion + 1);

        var lastExcursion = vacationDetails_excursionRecyclerView.findElements(By.className("android.widget.TextView")).getLast();

//        ((JavascriptExecutor) driver).executeScript("mobile: dragGesture", ImmutableMap.of(
//                "elementId", ((RemoteWebElement) lastVacation).getId(),
//                "endX", -400,
//                "endY", 0
//        ));

        ((JavascriptExecutor) driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
                "elementId", ((RemoteWebElement) lastExcursion).getId(),
                "direction", "left",
                "percent", 0.75
        ));
        driverWait.until(ExpectedConditions.visibilityOf(vacationDetails_excursionRecyclerView));

        int numOfAssociatedExcursionsAfterDelete = vacationDetails_excursionRecyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(numOfAssociatedExcursionsAfterAddSample-1, numOfAssociatedExcursionsAfterDelete);
        Assert.assertEquals(initialNumOfAssociatedExcursion, numOfAssociatedExcursionsAfterDelete);
    }

    @Test(priority = 4)
    public void notifyOnDateTest() {

    }
    @Test(priority = 5)
    public void shareYourVacationPlanTest() {

    }

}
