import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AppiumTest {
    static AndroidDriver driver;
    static WebDriverWait driverWait;
    static AppiumDriverLocalService service;

    @Before
    public void SetUp() {

        service = new AppiumServiceBuilder()
                .usingPort(4327)
                .build();
        service.start();

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", "android");
        caps.setCapability("automationName", "UIAutomator2");
        caps.setCapability("platformVersion", "14");
        caps.setCapability("deviceName", "Pixel 5 - API 34");
//        caps.setCapability("udid", "emulator-5554");
//        caps.setCapability("appPackage", "");
//        caps.setCapability("appActivity", ".Settings");
        caps.setCapability("app", "C:\\Users\\Quang Pham\\IdeaProjects\\vacation-away\\app\\debug\\app-debug.apk");
//        hide keyboard
        caps.setCapability("unicodeKeyboard", true);
        caps.setCapability("resetKeyboard", true);

        driver = new AndroidDriver(service.getUrl(), caps);
        driverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
        service.stop();
    }

    @Test
    public void appiumTest1() {
        String mainPage_btn_id = "com.example.quangpham_011025265:id/enterVacationList";
        String mainPage_message_id = "com.example.quangpham_011025265:id/textView2";
        String vacationList_recyclerView_id = "com.example.quangpham_011025265:id/vacationRecyclerView";
        String vacationList_menuOptions_xPath = "//android.widget.ImageView[@content-desc=\"More options\"]";
        String vacationList_menuOptions_addSampleVacation_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Add Sample Vacation\"]";
        String vacationList_menuOptions_deleteLastVacation_xPath = "//android.widget.TextView[@resource-id=\"com.example.quangpham_011025265:id/title\" and @text=\"Delete Last Vacation\"]";

        // Main Page
        var mainPageBtn = driver.findElement(By.id(mainPage_btn_id));
        var mainPageMessage = driver.findElement(By.id(mainPage_message_id));

        Assert.assertTrue(mainPageBtn.isDisplayed());
        Assert.assertTrue(mainPageMessage.isDisplayed());

        mainPageBtn.click();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(vacationList_recyclerView_id)));

        // Vacation List
        var vacationList_recyclerView = driver.findElement(By.id(vacationList_recyclerView_id));
        int numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();

        Assert.assertEquals(2, numOfVacation);

        var vacationList_menu = driver.findElement(By.xpath((vacationList_menuOptions_xPath)));
        vacationList_menu.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationList_menuOptions_addSampleVacation_xPath)));

        var vacationList_menuOptions_addSampleVacation = driver.findElement(By.xpath((vacationList_menuOptions_addSampleVacation_xPath)));
        vacationList_menuOptions_addSampleVacation.click();
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(4, numOfVacation);

        vacationList_menu.click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(vacationList_menuOptions_deleteLastVacation_xPath)));

        var vacationList_menuOptions_deleteLastVacation = driver.findElement(By.xpath((vacationList_menuOptions_deleteLastVacation_xPath)));
        vacationList_menuOptions_deleteLastVacation.click();
        driverWait.until(ExpectedConditions.visibilityOf(vacationList_recyclerView));

        numOfVacation = vacationList_recyclerView.findElements(By.className("android.widget.TextView")).size();
        Assert.assertEquals(3, numOfVacation);
    }
}