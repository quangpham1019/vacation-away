# **Vacation Away**

- The application allows user to add, update, and delete vacations, as well as managing the associated excursions.
- Users can also set reminder for android to push notification on the start date of a vacation / excursion.

## **PREVIEW**

#### **Main Activity**

![MainActivity.png](images/MainActivity.png)

#### **Vacation List**

- Vacation List activity & its menu options

![VacationList.png](images/VacationList.png)
![VacationListMenu.png](images/VacationListMenu.png)

- Swipe left on vacation to delete

![DeleteVacation1.png](images/DeleteVacation1.png)
![DeleteVacation2.png](images/DeleteVacation2.png)

#### **Vacation Details**

- Vacation Details activity & its menu options

![VacationDetails.png](images/VacationDetails.png)
![VacationDetailsMenu.png](images/VacationDetailsMenu.png)

#### **Excursion Details**

- Excursion Details activity & its menu options

![ExcursionDetails.png](images/ExcursionDetails.png)
![ExcursionDetailsMenu.png](images/ExcursionDetailsMenu.png)

#### **Notification**

- Push notification to android on the start date of current vacation

![VacationNotifyOnStart1.png](images/VacationNotifyOnStart1.png)
![VacationNotifyOnStart2.png](images/VacationNotifyOnStart2.png)

- Push notification to android on the start date of current excursion

![ExcursionNotify1.png](images/ExcursionNotify1.png)
![ExcursionNotify2.png](images/ExcursionNotify2.png)

#### **Share Vacation Details**

- Compose a message with details of current vacation in the chosen messaging platform

![ShareVacation1.png](images/ShareVacation1.png)
![ShareVacation2.png](images/ShareVacation2.png)
![ShareVacation3.png](images/ShareVacation3.png)
![ShareVacation4.png](images/ShareVacation4.png)
![ShareVacation5.png](images/ShareVacation5.png)

#### **Validation**

- Will invoke error when attempting to delete vacations with associated excursions

![DeleteVacationFail.png](images/DeleteVacationFail.png)

- Will invoke error if attempting to 

![VacationEndDateError1.png](images/VacationEndDateError1.png)
![VacationEndDateError2.png](images/VacationEndDateError2.png)
![VacationStartDateError.png](images/VacationStartDateError.png)
