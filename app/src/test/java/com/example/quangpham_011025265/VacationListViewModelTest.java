package com.example.quangpham_011025265;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import androidx.recyclerview.widget.RecyclerView;

import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.Database.Impl.ExcursionRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.Impl.VacationRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.VacationRepository;
import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.UI.Adapter.VacationAdapter;
import com.example.quangpham_011025265.ViewModel.VacationListViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RunWith(MockitoJUnitRunner.class)
public class VacationListViewModelTest {

    private VacationRepository vacationRepository;
    private ExcursionRepository excursionRepository;
    private VacationAdapter vacationAdapter;
    private VacationListViewModel vacationListViewModel;

    @Before
    public void setUp() {
        vacationRepository = mock(VacationRepository_CompletableFuture.class);
        excursionRepository = mock(ExcursionRepository_CompletableFuture.class);
        vacationAdapter = mock(VacationAdapter.class);
        vacationListViewModel = new VacationListViewModel(excursionRepository, vacationRepository, vacationAdapter);

        // Arrange
        // Act
        // Assert
    }

    @After
    public void tearDown() {

    }

    @Test
    public void InitializeSampleVacationTest() throws ExecutionException, InterruptedException {
        // Arrange
        // Act
        vacationListViewModel.InitializeSampleVacation();

        // Assert
        verify(vacationRepository, times(2)).insert(any(Vacation.class));
    }

    @Test
    public void retrieveAndSetVacationListToRecyclerViewTest() throws ExecutionException, InterruptedException {
        // Arrange

        // Act
        vacationListViewModel.retrieveAndSetVacationListToRecyclerView();

        // Assert
        verify(vacationRepository, times(1)).getAllVacations();
        verify(vacationAdapter, times(1)).setVacations(anyList());
    }

    @Test
    public void hasNoAssociatedExcursionTest() throws ExecutionException, InterruptedException {

        // Arrange
        Vacation vacation1 = new Vacation();
        vacation1.setVacationId(1);
        List<Excursion> associatedExcursionList1 = Arrays.asList(
                new Excursion("ex1", "01/01/2023", vacation1.getVacationId()),
                new Excursion("ex2", "02/01/2023", vacation1.getVacationId())
        );
        Mockito.lenient().when(excursionRepository.getAssociatedExcursions(vacation1.getVacationId())).thenReturn(associatedExcursionList1);

        Vacation vacation2 = new Vacation();
        vacation2.setVacationId(2);
        List<Excursion> associatedExcursionList2 = new ArrayList<>();
        Mockito.lenient().when(excursionRepository.getAssociatedExcursions(vacation2.getVacationId())).thenReturn(associatedExcursionList2);

        // Act
        boolean vacation1HasNoAssociatedExcursion = vacationListViewModel.hasNoAssociatedExcursion(vacation1);
        boolean vacation2HasNoAssociatedExcursion = vacationListViewModel.hasNoAssociatedExcursion(vacation2);

        // Assert
        verify(excursionRepository, times(2)).getAssociatedExcursions(anyInt());

        assertFalse(vacation1HasNoAssociatedExcursion);
        assertTrue(vacation2HasNoAssociatedExcursion);
    }
}
