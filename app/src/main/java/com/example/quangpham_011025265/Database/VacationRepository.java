package com.example.quangpham_011025265.Database;

import com.example.quangpham_011025265.Entity.Vacation;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface VacationRepository {
    //region VACATION REPOSITORY
    void insert(Vacation vacation);

    List<Vacation> getAllVacations() throws ExecutionException, InterruptedException;

    Vacation getVacationById(int vacationId) throws ExecutionException, InterruptedException;

    void update(Vacation vacation);

    void deleteVacationByID(int vacationId);

    void delete(Vacation vacation);
    //endregion
}
