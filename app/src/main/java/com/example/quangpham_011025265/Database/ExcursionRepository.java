package com.example.quangpham_011025265.Database;

import com.example.quangpham_011025265.Entity.Excursion;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface ExcursionRepository {
    //region EXCURSION REPOSITORY
    void insert(Excursion excursion);

    List<Excursion> getAssociatedExcursions(int vacationId) throws ExecutionException, InterruptedException;

    Excursion getExcursionById(int excursionId) throws ExecutionException, InterruptedException;

    List<Excursion> getAllExcursions() throws ExecutionException, InterruptedException;

    void deleteExcursionByID(int excursionId);

    void update(Excursion excursion);

    void delete(Excursion excursion);
    //endregion
}
