package com.example.quangpham_011025265.Database.Impl;

import android.app.Application;

import com.example.quangpham_011025265.DAO.VacationDAO;
import com.example.quangpham_011025265.Database.VacationDatabaseBuilder;
import com.example.quangpham_011025265.Database.VacationRepository;
import com.example.quangpham_011025265.Entity.Vacation;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class VacationRepository_CompletableFuture implements VacationRepository {
    private VacationDAO mVacationDAO;

    public VacationRepository_CompletableFuture(Application application) {
        VacationDatabaseBuilder db = VacationDatabaseBuilder.getDatabase(application);
        mVacationDAO = db.vacationDAO();
    }

    //region VACATION REPOSITORY
    @Override
    public void insert(Vacation vacation) {
        CompletableFuture.runAsync(() -> mVacationDAO.insert(vacation)).join();
    }

    @Override
    public List<Vacation> getAllVacations() throws ExecutionException, InterruptedException {

        CompletableFuture<List<Vacation>> completableFuture =
                CompletableFuture
                        .supplyAsync(() -> mVacationDAO.getAllVacations());

        return completableFuture.get();
    }

    @Override
    public Vacation getVacationById(int vacationId) throws ExecutionException, InterruptedException {

        CompletableFuture<Vacation> completableFuture =
                CompletableFuture
                        .supplyAsync(() -> mVacationDAO.getVacationById(vacationId));

        return completableFuture.get();
    }

    @Override
    public void update(Vacation vacation) {
        CompletableFuture.runAsync(() -> mVacationDAO.update(vacation)).join();
    }

    @Override
    public void deleteVacationByID(int vacationId) {
        CompletableFuture.runAsync(() -> mVacationDAO.deleteVacationById(vacationId)).join();
    }

    @Override
    public void delete(Vacation vacation) {
        CompletableFuture.runAsync(() -> mVacationDAO.delete(vacation)).join();
    }
    //endregion

}
