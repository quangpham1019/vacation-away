package com.example.quangpham_011025265.Database.Impl;

import android.app.Application;
import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.DAO.ExcursionDAO;
import com.example.quangpham_011025265.Database.VacationDatabaseBuilder;
import com.example.quangpham_011025265.Entity.Excursion;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ExcursionRepository_CompletableFuture implements ExcursionRepository {
    private ExcursionDAO mExcursionDAO;

    public ExcursionRepository_CompletableFuture(Application application) {
        VacationDatabaseBuilder db = VacationDatabaseBuilder.getDatabase(application);
        mExcursionDAO = db.excursionDAO();
    }

    //region EXCURSION REPOSITORY
    @Override
    public void insert(Excursion excursion) {
        CompletableFuture.runAsync(() -> mExcursionDAO.insert(excursion)).join();
    }

    @Override
    public List<Excursion> getAssociatedExcursions(int vacationId) throws ExecutionException, InterruptedException {
        CompletableFuture<List<Excursion>> completableFuture =
                CompletableFuture
                        .supplyAsync(() -> mExcursionDAO.getAssociatedExcursions(vacationId));

        return completableFuture.get();
    }

    @Override
    public Excursion getExcursionById(int excursionId) throws ExecutionException, InterruptedException {
        CompletableFuture<Excursion> completableFuture =
                CompletableFuture
                        .supplyAsync(() -> mExcursionDAO.getExcursionById(excursionId));

        return completableFuture.get();
    }

    @Override
    public List<Excursion> getAllExcursions() throws ExecutionException, InterruptedException {
        CompletableFuture<List<Excursion>> completableFuture =
                CompletableFuture
                        .supplyAsync(() -> mExcursionDAO.getAllExcursions());

        return completableFuture.get();
    }

    @Override
    public void deleteExcursionByID(int excursionId) {
        CompletableFuture.runAsync(() -> mExcursionDAO.deleteExcursionById(excursionId)).join();
    }

    @Override
    public void update(Excursion excursion) {
        CompletableFuture.runAsync(() -> mExcursionDAO.update(excursion)).join();
    }

    @Override
    public void delete(Excursion excursion) {
        CompletableFuture.runAsync(() -> mExcursionDAO.delete(excursion)).join();
    }
    //endregion

}