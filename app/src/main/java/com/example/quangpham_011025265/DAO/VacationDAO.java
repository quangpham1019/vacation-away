package com.example.quangpham_011025265.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quangpham_011025265.Entity.Vacation;

import java.util.List;

@Dao
public interface VacationDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Vacation vacation);
    @Update
    void update(Vacation vacation);
    @Delete
    void delete(Vacation vacation);
    @Query("SELECT * FROM VACATIONS ORDER BY vacationId ASC")
    List<Vacation> getAllVacations();
    @Query("SELECT * FROM VACATIONS WHERE vacationId=:vacationId")
    Vacation getVacationById(int vacationId);

    @Query("DELETE FROM VACATIONS WHERE vacationId=:vacationId")
    void deleteVacationById(int vacationId);
}
