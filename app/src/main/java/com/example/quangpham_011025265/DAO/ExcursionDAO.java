package com.example.quangpham_011025265.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.quangpham_011025265.Entity.Excursion;

import java.util.List;

@Dao
public interface ExcursionDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Excursion excursion);
    @Update
    void update(Excursion excursion);
    @Query("SELECT * FROM EXCURSIONS WHERE excursionId=:excursionId")
    Excursion getExcursionById(int excursionId);
    @Query("SELECT * FROM EXCURSIONS WHERE vacationId=:vacationId ORDER BY excursionId ASC")
    List<Excursion> getAssociatedExcursions(int vacationId);
    @Query("SELECT * FROM EXCURSIONS ORDER BY excursionId ASC")
    List<Excursion> getAllExcursions();
    @Query("DELETE FROM EXCURSIONS WHERE excursionId=:excursionId")
    void deleteExcursionById(int excursionId);
    @Delete
    void delete(Excursion excursion);
}
