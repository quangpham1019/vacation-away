package com.example.quangpham_011025265.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.Database.Impl.ExcursionRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.Impl.VacationRepository_CompletableFuture;
import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.R;
import com.example.quangpham_011025265.UI.Adapter.ExcursionAdapter;
import com.example.quangpham_011025265.UI.Adapter.VacationAdapter;
import com.example.quangpham_011025265.ViewModel.VacationDetailsViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class VacationDetails extends AppCompatActivity {

    private VacationDetailsViewModel vacationDetailsViewModel;
    private RecyclerView recyclerView;

    private EditText editVacationName, editVacationLodging;
    private TextView editStartDate, editEndDate;
    private ImageView imageDeleteStartDate, imageDeleteEndDate, imageEditStartDate, imageEditEndDate, imageAddExcursion;

    private DatePickerDialog.OnDateSetListener startDatePicker, endDatePicker;
    private final Calendar startDateCalendar = Calendar.getInstance();
    private final Calendar endDateCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_details);

        vacationDetailsViewModel = new VacationDetailsViewModel(
                new VacationRepository_CompletableFuture(getApplication()),
                new ExcursionRepository_CompletableFuture(getApplication()),
                new ExcursionAdapter(this)
        );

        InitializeViewVariables();
        InitializeRecyclerViewAndAssociatedExcursionList();
        if (vacationDetailsViewModel.curVacation != null) {
            LoadVacationFromIntent();
        }
        InitializeActivityAction();
    }

    private void InitializeViewVariables() {
        int vacationId = getIntent().getIntExtra("vacationId", -1);
        vacationDetailsViewModel.initializeCurVacation(vacationId);

        editVacationName = findViewById(R.id.editDetailsVacationName);
        editVacationLodging = findViewById(R.id.editDetailsVacationLodging);
        editStartDate = findViewById(R.id.editDetailsStartDate);
        editEndDate = findViewById(R.id.editDetailsEndDate);

        imageDeleteStartDate = findViewById(R.id.deleteStartDate);
        imageDeleteEndDate = findViewById(R.id.deleteEndDate);
        imageEditStartDate = findViewById(R.id.imageEditDetailsStartDate);
        imageEditEndDate = findViewById(R.id.imageEditDetailsEndDate);
        imageAddExcursion = findViewById(R.id.addExcursion);
    }
    private void InitializeRecyclerViewAndAssociatedExcursionList() {
        recyclerView = findViewById(R.id.excursionRecyclerView);

        // set adapter and layout manager for the recyclerView
        recyclerView.setAdapter(vacationDetailsViewModel.excursionAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        vacationDetailsViewModel.setAssociatedExcursionsToRecyclerView();
    }
    private void LoadVacationFromIntent() {
        String vacationNameFromIntent = getIntent().getStringExtra("vacationName");
        String vacationLodgingFromIntent = getIntent().getStringExtra("vacationLodging");
        String startDateFromIntent = getIntent().getStringExtra("startDate");
        String endDateFromIntent = getIntent().getStringExtra("endDate");

        editVacationName.setText(vacationNameFromIntent);
        editVacationLodging.setText(vacationLodgingFromIntent);
        editStartDate.setText(startDateFromIntent);
        editEndDate.setText(endDateFromIntent);
    }
    private void InitializeActivityAction() {
        // Initialize actions on add excursion, reset dates
        imageAddExcursion.setOnClickListener(view -> {
            if (saveVacation()) {
                    Intent intent = new Intent(VacationDetails.this, ExcursionDetails.class);
                    intent.putExtra("associatedVacationId", vacationDetailsViewModel.curVacation.getVacationId());
                    startActivity(intent);
            }
        });
        imageDeleteStartDate.setOnClickListener(view -> editStartDate.setText(""));
        imageDeleteEndDate.setOnClickListener(view -> editEndDate.setText(""));

        InitializeDatePicker();

        // Set up swipe to delete
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                int excursionAdapterPosition = viewHolder.getAdapterPosition();
                Excursion curExcursion = vacationDetailsViewModel.associatedExcursionList.get(excursionAdapterPosition);

                vacationDetailsViewModel.excursionRepository.delete(curExcursion);
                vacationDetailsViewModel.excursionAdapter.notifyItemRemoved(excursionAdapterPosition);
                Toast.makeText(VacationDetails.this, "Excursion Id " + curExcursion.getExcursionId() + " Deleted", Toast.LENGTH_LONG).show();
                onResume();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }
    private void InitializeDatePicker() {
        // SET UP datePicker
        // // General configuration
        String myFormat = "MM/dd/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        // // SET UP datePicker for editStartDate text view
        // // // DatePicker ensures valid start date format
        // // // Start date is validated to be before end date
        imageEditStartDate.setOnClickListener(view -> {
            String startDateFromScreen = editStartDate.getText().toString();
            if (startDateFromScreen.equals("mm/dd/yy") || startDateFromScreen.isEmpty()) {
                startDateFromScreen = sdf.format(new Date());
            }

            startDateCalendar.setTime(vacationDetailsViewModel.ParseDate(sdf, startDateFromScreen));
            new DatePickerDialog(VacationDetails.this, startDatePicker, startDateCalendar.get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH), startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
        startDatePicker = (datePicker, year, monthOfYear, dayOfMonth) -> {
            String endDateFromScreen = editEndDate.getText().toString();

            startDateCalendar.set(Calendar.YEAR, year);
            startDateCalendar.set(Calendar.MONTH, monthOfYear);
            startDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (!endDateFromScreen.equals("mm/dd/yy")
                    && !endDateFromScreen.isEmpty()
                    && !startDateCalendar.getTime().before(vacationDetailsViewModel.ParseDate(sdf, endDateFromScreen))) {
                Toast.makeText(VacationDetails.this, "Start Date must be before End Date", Toast.LENGTH_LONG).show();
            } else {
                editStartDate.setText(sdf.format(startDateCalendar.getTime()));
            }
        };

        // // SET UP datePicker for editEndDate text view
        // // // DatePicker ensures valid end date format
        // // // End date is validated to be after start date
        imageEditEndDate.setOnClickListener(view -> {
            String endDateFromScreen = editEndDate.getText().toString();
            if (endDateFromScreen.equals("mm/dd/yy") || endDateFromScreen.isEmpty()) {
                endDateFromScreen = sdf.format(new Date());
            }

            Date endDateFromScreenParsed = vacationDetailsViewModel.ParseDate(sdf, endDateFromScreen);
            endDateCalendar.setTime(endDateFromScreenParsed);

            new DatePickerDialog(VacationDetails.this, endDatePicker, endDateCalendar.get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH), endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
        endDatePicker = (datePicker, year, monthOfYear, dayOfMonth) -> {
            String startDateFromScreen = editStartDate.getText().toString();

            endDateCalendar.set(Calendar.YEAR, year);
            endDateCalendar.set(Calendar.MONTH, monthOfYear);
            endDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (!startDateFromScreen.equals("mm/dd/yy")
                    && !startDateFromScreen.isEmpty()
                    && !endDateCalendar.getTime().after(vacationDetailsViewModel.ParseDate(sdf, startDateFromScreen))) {
                Toast.makeText(VacationDetails.this, "End Date must be after Start Date", Toast.LENGTH_LONG).show();
            } else {
                editEndDate.setText(sdf.format(endDateCalendar.getTime()));
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vacation_details, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            this.finish();
        }

        if (menuItem.getItemId() == R.id.saveVacation) {
            if (saveVacation()) {
                this.finish();
            }
        }

        if (menuItem.getItemId() == R.id.notifyOnStartDate) {
            setNotificationTrigger("start", "starts today!");
        }

        if (menuItem.getItemId() == R.id.notifyOnEndDate) {
            setNotificationTrigger("end", "is ending today!");
        }

        if (menuItem.getItemId() == R.id.shareVacation) {
            if(saveVacation()) {
                Intent shareIntent = vacationDetailsViewModel.createShareIntent();
                startActivity(shareIntent);
            }
        }

        if (menuItem.getItemId() == R.id.addSampleExcursion) {
            if (saveVacation()) {
                vacationDetailsViewModel.addSampleExcursion();
                onResume();
            }
        }

        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();
        vacationDetailsViewModel.setAssociatedExcursionsToRecyclerView();
    }

    private boolean saveVacation() {

        String vacationName = editVacationName.getText().toString();
        String vacationLodging = editVacationLodging.getText().toString();
        String startDate = editStartDate.getText().toString();
        String endDate = editEndDate.getText().toString();

        if (!vacationDetailsViewModel.validateBeforeSave(
                vacationName,
                vacationLodging,
                startDate,
                endDate
        )) {
            Toast.makeText(this, "Please fill all information before proceeding", Toast.LENGTH_LONG).show();
            return false;
        }

        String saveConfirmationMessage = vacationDetailsViewModel.saveVacation(
                vacationName,
                vacationLodging,
                startDate,
                endDate);

        Toast.makeText(this, saveConfirmationMessage, Toast.LENGTH_LONG).show();
        return true;
    }
    private void setNotificationTrigger(String triggerPeriod, String notifyMessage) {

        if (!saveVacation()) return;

        String notifyDateString = null;
        switch (triggerPeriod) {
            case "start":
                notifyDateString = vacationDetailsViewModel.curVacation.getStartDate();
                break;
            case "end":
                notifyDateString = vacationDetailsViewModel.curVacation.getEndDate();
                break;
        }

        String myFormat = "MM/dd/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Date notifyDate = vacationDetailsViewModel.ParseDate(sdf, notifyDateString);
        long notifyDateTrigger = notifyDate.getTime();

        // // Generate the PENDING INTENT
        Intent notifyDateIntent = new Intent(VacationDetails.this, MyReceiver.class);
        notifyDateIntent.putExtra("key", "Your vacation - " + vacationDetailsViewModel.curVacation.getVacationName() + ", " + notifyMessage);

        PendingIntent notifyDateSender = PendingIntent.getBroadcast(VacationDetails.this, ++MainActivity.numAlert, notifyDateIntent, PendingIntent.FLAG_IMMUTABLE);

        // // Set the ALARM MANAGER to wake up on the trigger date
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, notifyDateTrigger, notifyDateSender);

        Toast.makeText(this, "Will notify vacation " + triggerPeriod + " on " + notifyDateString, Toast.LENGTH_SHORT).show();
    }
}