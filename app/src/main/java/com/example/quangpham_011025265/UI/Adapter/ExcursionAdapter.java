package com.example.quangpham_011025265.UI.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.R;
import com.example.quangpham_011025265.UI.ExcursionDetails;
import com.example.quangpham_011025265.UI.VacationDetails;

import java.util.List;

public class ExcursionAdapter extends RecyclerView.Adapter<ExcursionAdapter.ExcursionViewHolder> {
    private List<Excursion> mExcursions;
    private final Context context;
    private final LayoutInflater mInflater;
    public ExcursionAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }
    public class ExcursionViewHolder extends RecyclerView.ViewHolder {
        private final TextView excursionItemView;
        public ExcursionViewHolder(@NonNull View itemView) {
            super(itemView);

            // TODO: set onClick listener for the itemView
            // attach the view with id 'excursionItem' to variable 'excursionItemView'
            excursionItemView = itemView.findViewById(R.id.excursionItem);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    final Excursion currentExcursion = mExcursions.get(position);
                    Intent intent = new Intent(context, ExcursionDetails.class);
                    intent.putExtra("excursionId", currentExcursion.getExcursionId());
                    intent.putExtra("excursionName", currentExcursion.getExcursionName());
                    intent.putExtra("excursionDate", currentExcursion.getExcursionDate());
                    intent.putExtra("associatedVacationId", currentExcursion.getVacationId());
                    Toast.makeText(context, "Excursion Id: " + currentExcursion.getExcursionId() + " - Vacation Id: " + currentExcursion.getVacationId() , Toast.LENGTH_SHORT).show();
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public ExcursionAdapter.ExcursionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = mInflater.inflate(R.layout.excursion_list_item, parent, false);
        return new ExcursionAdapter.ExcursionViewHolder(itemView);
    }

    // Display info to the recycler view
    @Override
    public void onBindViewHolder(@NonNull ExcursionAdapter.ExcursionViewHolder holder, int position) {
        if (mExcursions != null) {
            Excursion currentExcursion = mExcursions.get(position);
            String name = currentExcursion.getExcursionName();
            holder.excursionItemView.setText(name);
        } else {
            holder.excursionItemView.setText("No excursion name");
        }
    }

    @Override
    public int getItemCount() {
        if(mExcursions != null) {
            return mExcursions.size();
        }
        else return 0;
    }

    public void setExcursions(List<Excursion> excursions) {
        mExcursions = excursions;
        notifyDataSetChanged();
    }
}
