package com.example.quangpham_011025265.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.Database.Impl.ExcursionRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.Impl.VacationRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.VacationRepository;
import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.R;
import com.example.quangpham_011025265.UI.Adapter.VacationAdapter;
import com.example.quangpham_011025265.ViewModel.VacationListViewModel;
import com.example.quangpham_011025265.ViewModel.VacationListViewModel.VacationDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class VacationList extends AppCompatActivity {

    private VacationListViewModel vacationListViewModel;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacation_list);

        vacationListViewModel = new VacationListViewModel(
                new ExcursionRepository_CompletableFuture(getApplication()),
                new VacationRepository_CompletableFuture(getApplication()),
                new VacationAdapter(this)
        );

        InitializeRecyclerView();
        InitializeActivityActions();
        if (vacationListViewModel.allVacations.isEmpty()) {
            vacationListViewModel.InitializeSampleVacation();
        }
    }

    public void InitializeRecyclerView() {
        recyclerView = findViewById(R.id.vacationRecyclerView);
        recyclerView.setAdapter(vacationListViewModel.vacationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        vacationListViewModel.retrieveAndSetVacationListToRecyclerView();
    }
    public void InitializeActivityActions() {
        ImageView addVacation = findViewById(R.id.addVacation);
        addVacation.setOnClickListener(view -> {
            Intent intent = new Intent(VacationList.this, VacationDetails.class);
            startActivity(intent);
        });

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Vacation currentVacation = vacationListViewModel.allVacations.get(viewHolder.getAdapterPosition());

                if (vacationListViewModel.hasNoAssociatedExcursion(currentVacation)) {
                    Toast.makeText(VacationList.this, "Vacation " + currentVacation.getVacationName() + " Deleted", Toast.LENGTH_LONG).show();
                    vacationListViewModel.vacationRepository.delete(currentVacation);
                    vacationListViewModel.vacationAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                    onResume();
                } else {
                    Toast.makeText(VacationList.this, "Cannot delete vacation with associated excursions", Toast.LENGTH_SHORT).show();
                    onResume();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vacation_list, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Check for invalid operations when there is no vacation
        // Only home navigation and 'Add Sample Vacation' are allowed when there is no sample vacation
        if(vacationListViewModel.allVacations.isEmpty()
                && menuItem.getItemId() != R.id.addSampleVacation
                && menuItem.getItemId() != android.R.id.home) {
            Toast.makeText(this, "Please add a sample vacation first", Toast.LENGTH_SHORT).show();
            return true;
        }

        // ADD 2 sample vacations to the database
        if (menuItem.getItemId() == R.id.addSampleVacation) {
            vacationListViewModel.InitializeSampleVacation();
            onResume();
        }

        // Navigate back to the main activity
        if (menuItem.getItemId() == android.R.id.home) {
            this.finish();
        }


        // Menu Options for quick testing

        if (menuItem.getItemId() == R.id.addExcursionToLastVacation) {
            Vacation lastInList = vacationListViewModel.allVacations.get(vacationListViewModel.allVacations.size()-1);

            Excursion excursion = new Excursion("Boat tour", lastInList.getStartDate(), lastInList.getVacationId());
            vacationListViewModel.excursionRepository.insert(excursion);
            lastInList.setVacationName(lastInList.getVacationName() + " + 1 excursion");
            vacationListViewModel.vacationRepository.update(lastInList);
            onResume();
        }

        if (menuItem.getItemId() == R.id.updateLastVacation) {
            Vacation lastInList = vacationListViewModel.allVacations.get(vacationListViewModel.allVacations.size()-1);

            lastInList.setVacationName("Updated " + lastInList.getVacationName());
            vacationListViewModel.vacationRepository.update(lastInList);
            onResume();

        }

        if (menuItem.getItemId() == R.id.deleteLastVacation) {
            Vacation lastInList = vacationListViewModel.allVacations.get(vacationListViewModel.allVacations.size()-1);

            if (vacationListViewModel.hasNoAssociatedExcursion(lastInList)){
                vacationListViewModel.vacationRepository.deleteVacationByID(lastInList.getVacationId());
                Toast.makeText(this, "Deleting last vacation", Toast.LENGTH_SHORT).show();
                onResume();
            } else {
                Toast.makeText(this, "Cannot delete vacation with associated excursions", Toast.LENGTH_SHORT).show();
            }

        }

        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();

        vacationListViewModel.retrieveAndSetVacationListToRecyclerView();
    }
}
