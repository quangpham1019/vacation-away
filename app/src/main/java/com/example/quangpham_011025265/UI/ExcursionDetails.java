package com.example.quangpham_011025265.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.Database.Impl.ExcursionRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.Impl.VacationRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.VacationRepository;
import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class ExcursionDetails extends AppCompatActivity {

    private VacationRepository vacationRepository;
    private ExcursionRepository excursionRepository;
    private Excursion currentExcursion;
    private Vacation associatedVacation;

    private TextView editAssociatedVacationId, editExcursionDate;
    private EditText editExcursionName;
    private ImageView imageEditExcursionDate, imageDeleteExcursionDate;

    private DatePickerDialog.OnDateSetListener excursionDatePicker;
    private final Calendar excursionDateCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excursion_details);

        vacationRepository = new VacationRepository_CompletableFuture(getApplication());
        excursionRepository = new ExcursionRepository_CompletableFuture(getApplication());

        InitializeViewVariables();
        InitializeActivityAction();
        InitializeDatePicker();
    }

    private void InitializeViewVariables() {
        editAssociatedVacationId = findViewById(R.id.editDetailsExcursionAssociatedVacation);
        editExcursionName = findViewById(R.id.editDetailsExcursionName);
        editExcursionDate = findViewById(R.id.editDetailsExcursionDate);
        imageEditExcursionDate = findViewById(R.id.imageEditDetailsExcursionDate);
        imageDeleteExcursionDate = findViewById(R.id.imageDeleteDetailsExcursionDate);

        int associatedVacationId = getIntent().getIntExtra("associatedVacationId", -1);
        int excursionId = getIntent().getIntExtra("excursionId", -1);
        try {
            associatedVacation = vacationRepository.getVacationById(associatedVacationId);
            currentExcursion = excursionRepository.getExcursionById(excursionId);
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (currentExcursion != null) {
            String excursionName = getIntent().getStringExtra("excursionName");
            String excursionDate = getIntent().getStringExtra("excursionDate");

            editExcursionName.setText(excursionName);
            editExcursionDate.setText(excursionDate);
        }

        editAssociatedVacationId.setText(String.valueOf(associatedVacation.getVacationId()));
    }
    private void InitializeActivityAction() {
        imageDeleteExcursionDate.setOnClickListener(view -> editExcursionDate.setText(""));

        InitializeDatePicker();
    }
    private void InitializeDatePicker() {
        String myFormat = "MM/dd/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        imageEditExcursionDate.setOnClickListener(view -> {
            String excursionDateFromScreen = editExcursionDate.getText().toString();
            if (excursionDateFromScreen.equals("mm/dd/yy")
                    || excursionDateFromScreen.isEmpty()) {
                excursionDateFromScreen = sdf.format(new Date());
            }

            excursionDateCalendar.setTime(ParseDate(sdf, excursionDateFromScreen));
            new DatePickerDialog(ExcursionDetails.this, excursionDatePicker, excursionDateCalendar.get(Calendar.YEAR), excursionDateCalendar.get(Calendar.MONTH), excursionDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
        excursionDatePicker = (datePicker, year, monthOfYear, dayOfMonth) -> {

            String startDateFromAssociatedVacation = associatedVacation.getStartDate();
            String endDateFromAssociatedVacation = associatedVacation.getEndDate();

            excursionDateCalendar.set(Calendar.YEAR, year);
            excursionDateCalendar.set(Calendar.MONTH, monthOfYear);
            excursionDateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (!excursionDateCalendar.getTime().before(ParseDate(sdf, startDateFromAssociatedVacation))
                    && !excursionDateCalendar.getTime().after(ParseDate(sdf, endDateFromAssociatedVacation))) {
                editExcursionDate.setText(sdf.format(excursionDateCalendar.getTime()));
            } else {
                Toast.makeText(
                        ExcursionDetails.this,
                        "Excursion Date must be between " + startDateFromAssociatedVacation + " - " + endDateFromAssociatedVacation,
                        Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_excursion_details, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        if (menuItem.getItemId() == android.R.id.home) {
            this.finish();
        }

        if (menuItem.getItemId() == R.id.saveExcursion) {
            saveExcursion();
        }

        if (menuItem.getItemId() == R.id.notifyOnExcursionDate) {
            setNotificationTrigger();
        }

        return true;
    }

    private boolean saveExcursion() {

        if (!validateDate(editExcursionDate.getText().toString())
                || !validateTitle(editExcursionName.getText().toString())) {
            Toast.makeText(this, "Please set title & date before proceeding", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (currentExcursion == null) {
            currentExcursion = new Excursion(
                    editExcursionName.getText().toString(),
                    editExcursionDate.getText().toString(),
                    Integer.parseInt(editAssociatedVacationId.getText().toString())
            );
            Toast.makeText(this, "Save new excursion", Toast.LENGTH_SHORT).show();
            excursionRepository.insert(currentExcursion);
        } else {

            currentExcursion.setExcursionName(editExcursionName.getText().toString());
            currentExcursion.setExcursionDate(editExcursionDate.getText().toString());
            Toast.makeText(this, "Updating excursion #" + currentExcursion.getExcursionId(), Toast.LENGTH_SHORT).show();
            excursionRepository.update(currentExcursion);
        }
        this.finish();
        return true;
    }
    private void setNotificationTrigger() {

        if (!saveExcursion()) return;

        String notifyDateString = currentExcursion.getExcursionDate();
        String myFormat = "MM/dd/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        Date notifyDate = ParseDate(sdf, notifyDateString);
        long notifyDateTrigger = notifyDate.getTime();

        // // Generate the PENDING INTENT
        Intent notifyDateIntent = new Intent(ExcursionDetails.this, MyReceiver.class);
        notifyDateIntent.putExtra("key", "Your excursion - " + currentExcursion.getExcursionName() + ", is scheduled for today!");

        PendingIntent notifyDateSender = PendingIntent.getBroadcast(ExcursionDetails.this, ++MainActivity.numAlert, notifyDateIntent, PendingIntent.FLAG_IMMUTABLE);

        // // Set the ALARM MANAGER to wake up on the trigger date
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, notifyDateTrigger, notifyDateSender);

        Toast.makeText(this, "Will notify excursion " + currentExcursion.getExcursionName() + " on " + notifyDateString, Toast.LENGTH_SHORT).show();
    }
    private boolean validateTitle(String title) {
        return !title.isEmpty();
    }
    private boolean validateDate(String date) {
        return !date.equals("mm/dd/yy")
                && !date.isEmpty();
    }
    private Date ParseDate(SimpleDateFormat sdf, String dateToParse) {
        Date date;

        try {
            date = sdf.parse(dateToParse);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return date;
    }
}