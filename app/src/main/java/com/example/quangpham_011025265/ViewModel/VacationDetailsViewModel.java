package com.example.quangpham_011025265.ViewModel;

import static androidx.core.content.ContextCompat.getSystemService;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.Database.Impl.ExcursionRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.Impl.VacationRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.VacationRepository;
import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.R;
import com.example.quangpham_011025265.UI.Adapter.ExcursionAdapter;
import com.example.quangpham_011025265.UI.ExcursionDetails;
import com.example.quangpham_011025265.UI.MainActivity;
import com.example.quangpham_011025265.UI.MyReceiver;
import com.example.quangpham_011025265.UI.VacationDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class VacationDetailsViewModel {

    public VacationRepository vacationRepository;
    public ExcursionRepository excursionRepository;
    public ExcursionAdapter excursionAdapter;
    public List<Excursion> associatedExcursionList;
    public Vacation curVacation;

    public VacationDetailsViewModel(VacationRepository vacationRepository, ExcursionRepository excursionRepository, ExcursionAdapter excursionAdapter) {
        this.vacationRepository = vacationRepository;
        this.excursionRepository = excursionRepository;
        this.excursionAdapter = excursionAdapter;
    }

    public void initializeCurVacation(int vacationId) {
        try {
            curVacation = vacationRepository.getVacationById(vacationId);
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    public Date ParseDate(SimpleDateFormat sdf, String dateToParse) {
        Date date;

        try {
            date = sdf.parse(dateToParse);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return date;
    }
    public String saveVacation(String vacationName, String vacationLodging, String startDate, String endDate) {

        if (curVacation == null) {
            curVacation = new Vacation(
                    vacationName,
                    vacationLodging,
                    startDate,
                    endDate
            );
            vacationRepository.insert(curVacation);
            return "New Vacation Saved!";
        }

        curVacation.setVacationName(vacationName);
        curVacation.setVacationLodging(vacationLodging);
        curVacation.setStartDate(startDate);
        curVacation.setEndDate(endDate);
        vacationRepository.update(curVacation);
        return "Vacation Id #" + curVacation.getVacationId() + " Updated";
    }
    public void setAssociatedExcursionsToRecyclerView() {
        int curVacationId = curVacation == null ? -1 : curVacation.getVacationId();
        try {
            associatedExcursionList = excursionRepository.getAssociatedExcursions(curVacationId);
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        // Set excursions for the view through excursionAdapter
        excursionAdapter.setExcursions(associatedExcursionList);
    }
    public Intent createShareIntent() {
        Intent toBeShare = new Intent();
        toBeShare.setAction(Intent.ACTION_SEND);
        toBeShare.putExtra(Intent.EXTRA_TITLE, "Share details on your vacation - " + curVacation.getVacationName());

        StringBuilder vacationDetailsMessage =
                new StringBuilder("Vacation Name: " + curVacation.getVacationName()
                        + "\nVacation Lodging: " + curVacation.getVacationLodging()
                        + "\nStart Date: " + curVacation.getStartDate()
                        + "\nEnd Date: " + curVacation.getEndDate());

        if (!associatedExcursionList.isEmpty()) {
            vacationDetailsMessage.append("\nAssociated Excursions: ");
            for (Excursion excursion : associatedExcursionList) {
                vacationDetailsMessage
                        .append("\n - ")
                        .append(excursion.getExcursionName())
                        .append(" on ")
                        .append(excursion.getExcursionDate());
            }
        }

        toBeShare.putExtra(Intent.EXTRA_TEXT, vacationDetailsMessage.toString());
        toBeShare.setType("text/plain");

        Intent shareIntent = Intent.createChooser(toBeShare, "Details on " + curVacation.getVacationName());

        return shareIntent;
    }
    public boolean validateDate(String date) {
        return !date.equals("mm/dd/yy")
                && !date.isEmpty();
    }
    public boolean validateBeforeSave(String vacationName, String vacationLodging, String startDate, String endDate) {
        return !vacationName.isEmpty()
                && !vacationLodging.isEmpty()
                && validateDate(startDate)
                && validateDate(endDate);
    }

    public void addSampleExcursion() {
        Excursion sampleExcursion = new Excursion("Hiking", curVacation.getStartDate(), curVacation.getVacationId());
        excursionRepository.insert(sampleExcursion);
    }
}
