package com.example.quangpham_011025265.ViewModel;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quangpham_011025265.Database.ExcursionRepository;
import com.example.quangpham_011025265.Database.Impl.ExcursionRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.Impl.VacationRepository_CompletableFuture;
import com.example.quangpham_011025265.Database.VacationRepository;
import com.example.quangpham_011025265.Entity.Excursion;
import com.example.quangpham_011025265.Entity.Vacation;
import com.example.quangpham_011025265.R;
import com.example.quangpham_011025265.UI.Adapter.VacationAdapter;
import com.example.quangpham_011025265.UI.VacationDetails;
import com.example.quangpham_011025265.UI.VacationList;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class VacationListViewModel {

    public ExcursionRepository excursionRepository;
    public VacationRepository vacationRepository;
    public VacationAdapter vacationAdapter;
    public List<Vacation> allVacations;

    public VacationListViewModel(ExcursionRepository excursionRepository, VacationRepository vacationRepository, VacationAdapter vacationAdapter) {
        this.excursionRepository = excursionRepository;
        this.vacationRepository = vacationRepository;
        this.vacationAdapter = vacationAdapter;
    }

    public void InitializeSampleVacation() {
        VacationDate vacationDate = InitializeVacationDate();

        vacationRepository.insert(
                new Vacation("Tahiti", "Tahiti Beach Resort", vacationDate.startDate, vacationDate.endDate));
        vacationRepository.insert(
                new Vacation( "Honeymoon", "Vacation Away Cabana Resort", vacationDate.startDate, vacationDate.endDate));
    }
    public void retrieveAndSetVacationListToRecyclerView() {
        try {
            allVacations = vacationRepository.getAllVacations();
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        vacationAdapter.setVacations(allVacations);
    }
    public boolean hasNoAssociatedExcursion(Vacation currentVacation) {
        List<Excursion> associatedExcursionList;
        try {
            associatedExcursionList = excursionRepository.getAssociatedExcursions(currentVacation.getVacationId());
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        return associatedExcursionList.isEmpty();
    }
    public VacationDate InitializeVacationDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
        String startDate = sdf.format(calendar.getTime());

        calendar.add(Calendar.DATE, 7);
        String endDate = sdf.format(calendar.getTime());

        return new VacationDate(startDate, endDate);
    }
    public class VacationDate {
        public String startDate;
        public String endDate;

        private VacationDate(String startDate, String endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
}
